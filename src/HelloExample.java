import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

public class HelloExample {


    private static final Logger logger = Logger.getLogger(HelloExample.class);

    public static void main(String args[]) {

        StringBuilder sorties = new StringBuilder();
        sorties.append("%n %d{yyyy-MM-dd HH:mm:ss} ");
        sorties.append("%p %C:%L - %m");

        PatternLayout layout = new PatternLayout(sorties.toString());
        ConsoleAppender appender = new ConsoleAppender(layout);
        logger.addAppender(appender);
        logger.setLevel(Level.DEBUG);

        logger.debug("This is debug");
        logger.info("This is info");
        logger.warn("This is warn");

    }
}
