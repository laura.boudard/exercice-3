package fr.cnam.foad.nfa035.fileutils.streaming.test;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;

class StreamingTestTest extends StreamingTest {

    File image;
    ImageByteArrayFrame media;
    ImageStreamingSerializer serializer;
    ByteArrayOutputStream deserializationOutput;
    ImageStreamingDeserializer deserializer;
    File extractedImage;

    @org.junit.jupiter.api.BeforeEach
    void setUp() throws Exception{
        image = new File("image originale");
        media = new ImageByteArrayFrame(new ByteArrayOutputStream());
        serializer = new ImageSerializerBase64StreamingImpl();
        deserializationOutput = new ByteArrayOutputStream();
        deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);
        extractedImage = new File("new");
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() throws Exception {
    }

    @org.junit.jupiter.api.Test
    void testMain() {
    }

    @org.junit.jupiter.api.Test
    void testDifference() {
    }

    @org.junit.jupiter.api.Test
    void testIndexOfDifference() {
    }
}